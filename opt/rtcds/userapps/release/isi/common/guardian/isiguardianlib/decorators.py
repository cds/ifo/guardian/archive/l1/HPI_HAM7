from guardian import GuardStateDecorator
from ezca.ligofilter import LIGOFilter, Mask

from . import util
from . import const
from . import damping
from . import isolation
from . import masterswitch
from . import watchdog
from .errormessage import ErrorMessage


def __get_are_in_preferred_state_decorator(filter_channel_names, button_names, filter_names):
    filter_module_names = filter_names

    class _are_in_preferred_state(GuardStateDecorator):
        def pre_exec(self):
            error_message = util.check_if_requested_filter_modules_loaded(filter_channel_names, button_names, filter_module_names)
            if error_message:
                util.report_filter_error(error_message)

    return _are_in_preferred_state


def __get_have_correct_gain_decorator(filter_channel_names, correct_gain):
    class _have_correct_gain(GuardStateDecorator):
        def pre_exec(self):
            error_message = util.check_if_filters_have_correct_gain(
                    filter_channel_names, correct_gain)
            if error_message:
                util.report_filter_error(error_message)

    return _have_correct_gain


def __get_loops_are_off_decorator(iso_or_damp):
    if const.CHAMBER_TYPE == 'HPI' and iso_or_damp == 'DAMP':
        return blank_decorator
    filter_channel_names = [iso_or_damp+'_'+dof for dof in const.DOF_LIST]
    class _loops_are_off(GuardStateDecorator):
        def pre_exec(self):
            for filter_name in filter_channel_names:
                ligo_filter = LIGOFilter(filter_name, ezca)
                if Mask() != ligo_filter.get_current_state_mask()\
                        or ligo_filter.GAIN.get() != 0:
                    util.report_filter_error(ErrorMessage("Expected filter '%s' to be completely cleared." % filter_name))

    return _loops_are_off


@util.check_arg(0, 'control_level', isolation.const.ISOLATION_CONSTANTS['LEVEL_FILTERS'])
@util.check_arg(2, 'boost', [True, False])
def isolation_loops_are_in_preferred_state(control_level, deg_of_free_list, boost):
    filter_names = isolation.const.ISOLATION_CONSTANTS['LEVEL_FILTERS'][control_level]['MAIN']
    if boost:
        filter_names += isolation.const.ISOLATION_CONSTANTS['LEVEL_FILTERS'][control_level]['BOOST']

    filter_channel_names = ['ISO_' + dof for dof in deg_of_free_list]

    return __get_are_in_preferred_state_decorator(
            filter_channel_names = filter_channel_names,
            button_names = isolation.const.ISOLATION_CONSTANTS['ONLY_ON_BUTTONS'],
            filter_names = filter_names)


def isolation_loops_have_correct_gain(deg_of_free_list):
    return __get_have_correct_gain_decorator(
                filter_channel_names = ['ISO_' + dof for dof in deg_of_free_list],
                correct_gain = isolation.const.ISOLATION_CONSTANTS['RAMP_UP_GAINS'][-1],
                )


isolation_loops_are_off = __get_loops_are_off_decorator('ISO')


def blank_decorator(func):
    def wrapper(*args, **kwargs):
        return func(*args, **kwargs)
    return wrapper


damping_loops_are_in_preferred_state = __get_are_in_preferred_state_decorator(
                filter_channel_names = ['DAMP_'+dof for dof in damping.const.DAMPING_CONSTANTS['ALL_DOF']],
                button_names = damping.const.DAMPING_CONSTANTS['ONLY_ON_BUTTONS'],
                filter_names = damping.const.DAMPING_CONSTANTS['FILTER_NAMES'],
                ) if const.CHAMBER_TYPE != 'HPI' else blank_decorator


damping_loops_have_correct_gain = __get_have_correct_gain_decorator(
                filter_channel_names = ['DAMP_'+dof for dof in damping.const.DAMPING_CONSTANTS['ALL_DOF']],
                correct_gain = damping.const.DAMPING_CONSTANTS['GAIN'],
                ) if const.CHAMBER_TYPE != 'HPI' else blank_decorator


damping_loops_are_off = __get_loops_are_off_decorator('DAMP')


class watchdog_is_not_tripped(GuardStateDecorator):
    def pre_exec(self):
        watchdog_state = watchdog.util.read_watchdog_state()

        if watchdog_state != watchdog.const.WATCHDOG_ARMED_STATE:
            # FIXME We have to explicity write out jump state names
            # for the dumb jump edge parser. The preferred return is
            # of course
            # return 'watchdog.const.WATCHDOG_TRIPPED_%s' % watchdog.const.WATCHDOG_STATE_NAME[watchdog_state]
            if watchdog_state == watchdog.const.WATCHDOG_DEISOLATING_STATE:
                return 'WATCHDOG_TRIPPED_DEISOLATING'
            if watchdog_state == watchdog.const.WATCHDOG_DAMPING_STATE:
                return 'WATCHDOG_TRIPPED_DAMPING'
            if watchdog_state == watchdog.const.WATCHDOG_FULL_SHUTDOWN_STATE:
                return 'WATCHDOG_TRIPPED_FULL_SHUTDOWN'


class masterswitch_is_on(GuardStateDecorator):
    def pre_exec(self):
        if masterswitch.util.is_masterswitch_off():
            return 'MASTERSWITCH_OFF'
