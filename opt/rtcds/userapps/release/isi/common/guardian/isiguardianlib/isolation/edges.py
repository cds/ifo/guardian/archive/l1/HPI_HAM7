from . import const as iso_const
from .. import const as top_const
from . import util as iso_util


# The following code figures out how to glue together the isolation states that
# are defined according to the isolation.const module. In short, it creates the
# edges one would expect. That is, when there are Cartesian biases that should be
# restored between the engaging/ramping states for two sets of degrees of
# freedoms, a state to restore those Cartesian biases is inserted between those
# engaging/ramping states; when such a bias restoring state is not needed, that
# state is not inserted. The name of each state corresponds to the degrees of
# freedom that are changed by that state.

edges = [
    ('DEISOLATING', 'LOAD_CART_BIAS_FOR_ISOLATION'),
    ]

if 'ISO_OK_SENSOR' in iso_const.ISOLATION_CONSTANTS:
    last_pre_iso_state = 'WAIT_FOR_'+iso_const.ISOLATION_CONSTANTS['ISO_OK_SENSOR']+'_SETTLE'
    edges.append(('LOAD_CART_BIAS_FOR_ISOLATION', last_pre_iso_state))
else:
    last_pre_iso_state = 'LOAD_CART_BIAS_FOR_ISOLATION'

for control_level in iso_const.ISOLATION_CONSTANTS['LEVEL_FILTERS']:
    degs_of_freedom = iso_util.get_degs_of_freedom_string(iso_const.ISOLATION_CONSTANTS['DOF_LISTS'][0])

    edges.append((last_pre_iso_state,
            'ENGAGE_ISO_FILTERS_{control_level}_{degs_of_freedom}'.format(
                degs_of_freedom=degs_of_freedom, control_level=control_level)))

    for i in range(len(iso_const.ISOLATION_CONSTANTS['DOF_LISTS'])):
        degs_of_freedom = iso_util.get_degs_of_freedom_string(iso_const.ISOLATION_CONSTANTS['DOF_LISTS'][i])
        edges += [('ENGAGE_ISO_FILTERS_{control_level}_{degs_of_freedom}'.format(
                    degs_of_freedom=degs_of_freedom,control_level=control_level),
                   'RAMP_ISO_FILTERS_UP_{control_level}_{degs_of_freedom}'.format(
                    degs_of_freedom=degs_of_freedom, control_level=control_level)),
                  ('RAMP_ISO_FILTERS_UP_{control_level}_{degs_of_freedom}'.format(
                    degs_of_freedom=degs_of_freedom, control_level=control_level),
                   'ENGAGE_ISO_BOOST_{control_level}_{degs_of_freedom}'.format(
                    degs_of_freedom=degs_of_freedom, control_level=control_level)),
                  ]

        cart_bias_degs_of_freedom = iso_util.get_degs_of_freedom_string(iso_const.ISOLATION_CONSTANTS['CART_BIAS_DOF_LISTS'][i])

        if i >= len(iso_const.ISOLATION_CONSTANTS['DOF_LISTS']) - 1:
            if not cart_bias_degs_of_freedom:
                edges.append(('ENGAGE_ISO_BOOST_{control_level}_{degs_of_freedom}'.format(
                        degs_of_freedom=degs_of_freedom, control_level=control_level),
                    '{control_level}_ISOLATED'.format(control_level=control_level)))
            else:
                edges += [
                    ('ENGAGE_ISO_BOOST_{control_level}_{degs_of_freedom}'.format(
                        degs_of_freedom=degs_of_freedom, control_level=control_level),
                     'RESTORE_ISO_CART_BIAS_{control_level}_{degs_of_freedom}'.format(
                        degs_of_freedom=cart_bias_degs_of_freedom, control_level=control_level)),
                    ('RESTORE_ISO_CART_BIAS_{control_level}_{degs_of_freedom}'.format(
                        degs_of_freedom=cart_bias_degs_of_freedom, control_level=control_level),
                     '{control_level}_ISOLATED'.format(control_level=control_level)),
                    ]

        else:
            next_degs_of_freedom = iso_util.get_degs_of_freedom_string(iso_const.ISOLATION_CONSTANTS['DOF_LISTS'][i+1])

            if not cart_bias_degs_of_freedom:
                edges.append(('ENGAGE_ISO_BOOST_{control_level}_{degs_of_freedom}'.format(
                        degs_of_freedom=degs_of_freedom, control_level=control_level),
                    'ENGAGE_ISO_FILTERS_{control_level}_{degs_of_freedom}'.format(
                        degs_of_freedom=next_degs_of_freedom, control_level=control_level)))
            else:
                edges += [
                    ('ENGAGE_ISO_BOOST_{control_level}_{degs_of_freedom}'.format(
                        degs_of_freedom=degs_of_freedom, control_level=control_level),
                     'RESTORE_ISO_CART_BIAS_{control_level}_{degs_of_freedom}'.format(
                        degs_of_freedom=cart_bias_degs_of_freedom, control_level=control_level)),
                    ('RESTORE_ISO_CART_BIAS_{control_level}_{degs_of_freedom}'.format(
                        degs_of_freedom=cart_bias_degs_of_freedom, control_level=control_level),
                     'ENGAGE_ISO_FILTERS_{control_level}_{degs_of_freedom}'.format(
                        degs_of_freedom=next_degs_of_freedom, control_level=control_level)),
                    ]


    last = len(iso_const.ISOLATION_CONSTANTS['DOF_LISTS'])-1
    edges += [('{control_level}_ISOLATED'.format(
                control_level=control_level),
               'DISENGAGE_BOOST_{control_level}_{degs_of_freedom}'.format(
                control_level=control_level,
                degs_of_freedom=iso_util.get_degs_of_freedom_string(iso_const.ISOLATION_CONSTANTS['ALL_DOF']),
                ),)]

    edges.append(('DISENGAGE_BOOST_{control_level}_{degs_of_freedom}'.format(
                   control_level=control_level,
                   degs_of_freedom=iso_util.get_degs_of_freedom_string(iso_const.ISOLATION_CONSTANTS['ALL_DOF']),
                   ), 'DEISOLATING'))
